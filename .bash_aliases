#!/bin/bash
# alias.sh

ALIAS_FILE=$BASH_SOURCE
# `code` is for VSCode. Can replace with your fav editor.
alias aliases="code $ALIAS_FILE"
alias reload_aliases="source $ALIAS_FILE"

# Must set this option, else script will not expand aliases.
shopt -s expand_aliases

# More beautiful ACK command
alias acl='ack --pager="less -FRSX"'

data_usage() {
    du -h --max-depth=1 $1 2>&1 | grep -v 'denied' | sort -hr
}

# Creates a new Tmux session (but will attach if already exists), using the given name
atmux () { tmux new-session -A -s $1; }

# Creates a new "trash" folder if not already exists. If file/folder being trashed already exists, then filename increments.
del() { mkdir -p ~/.trash; mv --backup=numbered --target-directory ~/.trash/ $1; }
# Removes all files/folders in the "trash" directory
emptytrash() { rm -rf ~/.trash/*; }
