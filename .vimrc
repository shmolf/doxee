" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
" ------ Begin Vundle Plugins ------
set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'ajmwagar/vim-deus' " Good lookin color scheme
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ntpeters/vim-better-whitespace' " Highlighted trailing whitespace and :StripWWhitespace
Plugin 'lumiliet/vim-twig'
Plugin 'qpkorr/vim-bufkill'
"Plugin 'ajh17/VimCompletesMe' " I disabled so I could use shift+tab
Plugin 'ryanoasis/vim-devicons'
Plugin 'scrooloose/nerdtree'

" Plugin 'sheerun/vim-polyglot' " Supposed to be a multi-language package manager
Plugin 'phleet/vim-mercenary'
"Plugin 'swekaj/php-foldexpr.vim' " Can slow down vim scrolling
"Plugin 'isruslan/vim-es6' " For some reason, fixes PHP folding, but breaks JS folding
"Plugin 'pangloss/vim-javascript'

" mm ~ Add/remove bookmark at current line ~ :BookmarkToggle
" mi ~ Add/edit/remove annotation at current line ~ :BookmarkAnnotate <TEXT>
" mn ~ Jump to next bookmark in buffer ~ :BookmarkNext
" mp ~ Jump to previous bookmark in buffer ~ :BookmarkPrev
" ma ~ Show all bookmarks (toggle) ~ :BookmarkShowAll
" mc ~ Clear bookmarks in current buffer only ~ :BookmarkClear
" mx ~ Clear bookmarks in all buffers ~ :BookmarkClearAll
" [count]mkk ~ Move up bookmark at current line ~ :BookmarkMoveUp [<COUNT>]
" [count]mjj ~ Move down bookmark at current line ~ :BookmarkMoveDown [<COUNT>]
" [count]mg ~ Move bookmark at current line to another line ~ :BookmarkMoveToLine <LINE>
" Save all bookmarks to a file ~ no shortcut ~ :BookmarkSave <FILE_PATH>
" Load bookmarks from a file ~ no shortcut ~ :BookmarkLoad <FILE_PATH>
Plugin 'MattesGroeger/vim-bookmarks'

"Plugin 'rayburgemeestre/phpfolding.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" ------ End Vundle Plugins ------

set t_Co=256
" Commenting out, since seemed to break non-tmux sessions
" if (has("termguicolors"))
"    set termguicolors
" endif

" --- Plugin Settings Start ---

" White Space highlighting
highlight ExtraWhitespace ctermbg=Blue

" VimCompletesMe settings
"let b:vcm_tab_complete = 'omni'

" Set the airline theme
let g:airline_theme='distinguished'

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" Vim-Deus settings
set background=dark " Setting dark mode
colorscheme deus
let g:deus_termcolors=256

" Bookmark settings
highlight BookmarkSign ctermbg=NONE ctermfg=160
highlight BookmarkLine ctermbg=194 ctermfg=NONE
let g:bookmark_sign = '♥'
let g:bookmark_highlight_lines = 1

" --- Plugin Settings End ---

set encoding=utf8
"set omnifunc=syntaxcomplete#Complete " Use syntax completion specific to each programming language with Ctrl-X, Ctrl-O
if &t_Co > 2
   syntax on " Turn on syntax highlighting, if the terminal has colors
endif

" Tab and space options
set tabstop=3      " makes each tab character equal to 3 spaces
set shiftwidth=3   " makes auto-indent and (<<, >>) commands use 3 spaces for each step
set expandtab      " causes tab key to insert 3 spaces
set smarttab       " causes tab in front of the line to insert 3 spaces
set softtabstop=3  " makes spaces feel like a tab
set smartindent    " Automatically indent inside braces

" Search
set incsearch " Do incremental searching
set nohls " Do not highlight the last used search pattern
set nowrapscan " Do not wrap to beginning of a file when searching for a string

" Other
set backspace=indent,eol,start " Allow backspacing over everything in insert mode
set history=50 " Keep 50 lines of command line history
set ruler " Show the cursor position all the time
set scrolloff=5 " Show at least 5 lines of context above and below the cursor
set showcmd " Display information about characters and lines selected while selecting them
set number " Display line numbers on the left hand side
let loaded_matchparent = 1 " Turn off parenthesis matching
set ls=2 " Always display the path for the file you are editing.
set nopaste " Makes pasting so much better.
set pastetoggle=<f5>
set mouse=r " This is to not include line numbers when highlight text
set cursorline " Shows which line the cursor is on.
set foldmethod=syntax " Set the folding to syntax-based
let javaScript_fold=1 "activate folding by JS syntax
"let php_folding=1  "activate folding by PHP syntax
set nofoldenable " Set folding off, until turned on
set foldlevel=50 " Set a high fold level, so when folding is turned on, doesn't collapse everything
set hidden " Allow buffer switch even with changes in current buffer

" Line wrap
set textwidth=130
set colorcolumn=130
highlight ColorColumn ctermbg=0 guibg=lightgrey

" Highlight text past end of line in reverse color.
" Note that ctermbg must use an available color word for the terminal.
augroup vimrc_autocmds
  autocmd BufEnter * highlight OverLength ctermbg=black ctermfg=white guibg=black guifg=white
  autocmd BufEnter * match Error /\%121v.*/
  "autocmd BufEnter * match OverLength /\%121v.*/
  autocmd BufNewFile,BufRead *.spec   set syntax=xml
augroup END

" -- Remapping keys
"  nmap is for handling keys in Normal Mode
"  imap is for handling keys in Insert Mode
"  vmap is for handling keys in Visual Mode
"  map is for handling keys in All Modes

" Map buffer navigation
" Mappings to access buffers (don't use "\p" because a
" delay before pressing "p" would accidentally paste).
" \l       : list buffers
" \b \f \g : go back/forward/last-used
" \1 \2 \3 : go to buffer 1/2/3 etc
" \c \t    : close tab, create tab
" \w       : create window
nnoremap <Leader>l :ls<CR>
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>f :bn<CR>
nnoremap <Leader>e :e#<CR>
nnoremap <Leader>c :bd<CR>
nnoremap <Leader>t :enew<CR> :Lexplore<CR>
nnoremap <Leader>w :new<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

" Browser Directory
"map <C-n> :Lexplore<CR> " Browser Directory
map <C-n> :NERDTreeToggle<CR>
let g:netrw_winsize = 25
" Below will remap the numpad enter key, as a new line
map <kEnter> <CR>
" Below maps Ctrl-S as escaping the current mode, and saving
map <C-s> <ESC>:w<CR>
" Below maps shift-tab as deleting a tab
"imap <S-Tab> <BS>
"vmap <S-Tab> <ESC>i<BS>
"nmap <S-Tab> i<BS>
nnoremap <F6> :call ToggleMouse()<CR>
nnoremap <F7> :call ToggleNumbers()<CR>
function! ToggleMouse()
    " check if mouse is enabled
    if &mouse == 'a'
        set mouse=
        echo "Mouse disabled"
    else
        set mouse=a
        echo "Mouse enabled"
    endif
endfunc
function! ToggleNumbers()
   " Check if Numbers are enabled
   set invnumber
endfunc

" Get Blame information for Git, SVN, and HG
" \b for SVN
" \g for Git
" \h for HG
vmap <Leader>s :<C-U>!svn blame <C-R>=expand("%:p") <CR> \| sed -n <C-R>=line("'<") <CR>,<C-R>=line("'>") <CR>p <CR>
vmap <Leader>g :<C-U>!git blame <C-R>=expand("%:p") <CR> \| sed -n <C-R>=line("'<") <CR>,<C-R>=line("'>") <CR>p <CR>
vmap <Leader>h :<C-U>!hg blame -fu <C-R>=expand("%:p") <CR> \| sed -n <C-R>=line("'<") <CR>,<C-R>=line("'>") <CR>p <CR>
" This replaces the Quit command. Instead, it'll close the active file. This way, don't accidently close other tabs.
" When done, use 'CTRL-w q'to actually quit.
cabbrev q <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'BD' : 'q')<CR>
map <C-q> :hide<CR>
