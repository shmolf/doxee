# doxee

To append to your `.ackrc`,
```bash
wget https://gitlab.com/shmolf/doxee/-/raw/master/.ackrc -O ->> ~/.ackrc
```

To append to your `.bashrc`,
```bash
wget https://gitlab.com/shmolf/doxee/-/raw/master/.bashrc -O ->> ~/.bashrc
```

To full-on replace/setup your `.tmux.conf`
```bash
wget -O ~/.tmux.conf https://gitlab.com/shmolf/doxee/-/raw/master/.tmux.conf
```

To full-on replace/setup your `.vimrc`
```bash
wget -O ~/.vimrc https://gitlab.com/shmolf/doxee/-/raw/master/.vimrc && \
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim && \
vim +PluginInstall +qall
```
