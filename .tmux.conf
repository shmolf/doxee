# start window numbers at 1 to match keyboard order with tmux window order
set -g base-index 1
set-window-option -g pane-base-index 1

# renumber windows sequentially after closing any of them
set -g renumber-windows on

# increase scrollback lines
set -g history-limit 10000

# switch to last pane
bind-key C-a last-pane

# hotkey for synchronizing panels
bind-key -n F2 set-window-option synchronize-panes
bind-key -n F3 previous-layout
bind-key -n F4 next-layout

# functional hotkeys
#bind-key -n F7 detach-client
#bind-key -n F8 kill-window
#bind-key -n F9 new-window
#bind-key -n F10 command-prompt 'rename-window %%'
#bind-key -n F11 previous-window
#bind-key -n F12 next-window

# reload ~/.tmux.conf using PREFIX r
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# use ALT+Arrow to navigate panes
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

#Resize Window shortcuts
bind j resize-pane -D 8
bind k resize-pane -U 8
bind h resize-pane -L 8
bind l resize-pane -R 8

# Fuzzy session switching
bind C-j split-window -v "tmux list-sessions | sed -E 's/:.*$//' | grep -v \"^$(tmux display-message -p '#S')\$\" | fzf --reverse | xargs tmux switch-client -t"

# Quick session switching back and forth. I never use the built in layouts
# # (default mapping for Space), so I reclaimed the key for this
bind-key Space switch-client -l

# Make spilts make sense
bind | split-window -h
bind _ split-window -v

# allow mouse selection, only works on tmux 2.1+
#set -g mouse on

# fixes italics/reverse issue with tmux
set -g terminal-overrides 'rxvt-unicode*:sitm@,ritm@'

# Laptop options
#
# battery status
# ♥(acpi | cut -d ',' -f 2)
#
# audio status
# ♪ (exec amixer get Master | egrep -o "[0-9]+%" | egrep -o "[0-9]*")
#

# Display a little info
set -g status-interval 5
set -g status-left "#{?pane_synchronized,--SYNCED--,} | #S | "
set -g status-left-length 50
set -g status-right ' | %a %m-%d %H:%M |'
set -g status-justify centre

# No delay for escape
set -s escape-time 0

# Turn on window titles
set -g set-titles on

# Set window title string
#  #H  Hostname of local host
#  #I  Current window index
#  #P  Current pane index
#  #S  Session name
#  #T  Current window title
#  #W  Current window name
#  #   A literal ‘#’
set -g set-titles-string '#S: #W:#I'

# Automatically set window title
setw -g automatic-rename

# Set action on window bell. any means a bell in any window linked to a
# session causes a bell in the current window of that session, none means
# all bells are ignored and current means only bell in windows other than
# the current window are ignored.
set-option -g bell-action any

## solarize
## default statusbar colors
#set-option -g status-bg colour235 #base02
#set-option -g status-fg colour136 #yellow
#set-option -g status-attr default

## default window title colors
#set-window-option -g window-status-fg colour244 #base0
#set-window-option -g window-status-bg default
##set-window-option -g window-status-attr dim

## active window title colors
#set-window-option -g window-status-current-fg colour166 #orange
#set-window-option -g window-status-current-bg default
##set-window-option -g window-status-current-attr bright

## pane border
#set-option -g pane-border-fg colour235 #base02
#set-option -g pane-active-border-fg colour240 #base01

## message text
#set-option -g message-bg colour235 #base02
#set-option -g message-fg colour166 #orange

# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

# clock
set-window-option -g clock-mode-colour colour64 #green

setw -g aggressive-resize on

# List of plugins
# Supports `github_username/repo` or full git repo URLs
set -g @tpm_plugins '              \
  tmux-plugins/tpm                 \
  tmux-plugins/tmux-sensible       \
  tmux-plugins/tmux-resurrect      \
  tmux-plugins/tmux-sessionist     \
  tmux-plugins/tmux-yank           \
  tmux-plugins/tmux-copycat        \
  tmux-plugins/tmux-continuum      \
'
#  seebi/tmux-colors-solarized      \

# Theme
#set -g @colors-solarized 'dark'

# Other examples:
# github_username/plugin_name    \
# git@github.com/user/plugin     \
# git@bitbucket.com/user/plugin  \

set -g @continuum-restore 'on'
set -g @continuum-save-interval '240'
set -g @resurrect-processes 'ssh mysql'

# Initializes TMUX plugin manager.
# Keep this line at the very bottom of tmux.conf.
run-shell '~/.tmux/plugins/tpm/tpm'

