# This will stop the 'CTRL-C' combination from stopping terminal input
stty -ixon

function print_separator() {
   printf '\e[36m'
   rightnow="$(date '+%Y-%m-%d %H:%M') "
   gitBranch="$(git branch 2>/dev/null | grep "^*" | colrm 1 2)"
   conText="$rightnow"
   if [ ${#gitBranch} \> 0 ]; then
      conText="$conText($gitBranch) "
   fi
   textLen=${#conText}
   printf "$conText"
   for ((i = $textLen ; i < $COLUMNS ; i++)); do
      printf '─'
   done
   printf '\e[m\n'
}
PROMPT_COMMAND+="print_separator;"

export PS1="┌\[\033[38;5;11m\]╼\[$(tput sgr0)\] \[\033[38;5;76m\]\u\[$(tput sgr0)\] on \h: \[\033[38;5;64m\]\W\[$(tput sgr0)\]\n└╼┫ "
